const nodemailer = require('nodemailer')
const { google } = require('googleapis')

const { OAuth2 } = google.auth

// CRED: https://stackoverflow.com/a/34015511/1048518
const getPrettyNow = now => {
  const localTime = new Date(now)
  const dateFormatOpts = {
    month: 'short',
    day: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
  }

  return localTime.toLocaleDateString('en-US', dateFormatOpts)
}

exports.handler = (event, context, callback) => {
  const {
    GMAIL_CLIENT_ID,
    GMAIL_CLIENT_SECRET,
    GMAIL_REFRESH_TOKEN,
    GMAIL_USER,
  } = process.env

  const oauth2Client = new OAuth2(
    GMAIL_CLIENT_ID, // ClientID
    GMAIL_CLIENT_SECRET, // Client Secret
    'https://developers.google.com/oauthplayground' // Redirect URL
  )

  oauth2Client.setCredentials({
    refresh_token: GMAIL_REFRESH_TOKEN,
  })
  const accessToken = oauth2Client.getAccessToken()
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    auth: {
      user: GMAIL_USER,
      type: 'OAuth2',
      clientId: GMAIL_CLIENT_ID,
      clientSecret: GMAIL_CLIENT_SECRET,
      refreshToken: GMAIL_REFRESH_TOKEN,
      accessToken,
    },
  })

  transporter.sendMail(
    {
      from: '"ABM Contact Form" <jason@abettermap.com>',
      to: 'jason@abettermap.com',
      subject: `Map interest @ ${getPrettyNow(new Date())}`,
      text: event.body,
    },
    function handleMailError(error, info) {
      // TODO: try/catch or other error handling here AND AWS side
      // https://itnext.io/error-handling-with-async-await-in-js-26c3f20bc06a
      // http://thecodebarbarian.com/async-await-error-handling-in-javascript.html
      // https://dev.to/sobiodarlington/better-error-handling-with-async-await-2e5m
      // https://javascript.info/async-await
      if (error) {
        // TODO: return 400 or similar?
        callback(error)
      } else {
        callback(null, {
          statusCode: 200,
          body: JSON.stringify({
            ok: true,
          }),
        })
      }
    }
  )
}
