/* eslint-disable react/no-danger */
/* eslint-disable import/prefer-default-export */

/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

// NOTE: was never able to get any env's besides NODE_ENV to work for the env
// check, even with env vars like Netlify's CONTEXT. Had to apply a hardcoded
// hostname filter for www.abettermap.com in Google Analytics to view
// production-only tracking. The code below at least prevents tracking during
// local dev. If non-prod-non-local tracking is ever needed, will need to remove
// or adjust GA filter/s.

const React = require('react')

const GoogleTagManager = () => {
  const danger = {
    __html: `
      (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, 'script', 'dataLayer', 'GTM-TD6K4ND');
    `,
  }

  return <script dangerouslySetInnerHTML={danger} />
}

const GtagMgrNoScript = () => (
  <noscript>
    <iframe
      src="https://www.googletagmanager.com/ns.html?id=GTM-TD6K4ND"
      height="0"
      width="0"
      style={{ display: 'none', visibility: 'hidden' }}
    />
  </noscript>
)

export const onPreRenderHTML = ({
  getHeadComponents,
  replaceHeadComponents,
  getPreBodyComponents,
  replacePreBodyComponents,
}) => {
  const headComponents = getHeadComponents()

  headComponents.unshift(
    <meta name="apple-mobile-web-app-capable" content="yes" />
  )

  if (process.env.NODE_ENV !== `production`) {
    return null
  }

  headComponents.unshift(<GoogleTagManager />)
  replaceHeadComponents(headComponents)
  replacePreBodyComponents([<GtagMgrNoScript />, getPreBodyComponents()])

  return false
}
