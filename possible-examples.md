# Possible examples
Waaay more than mine.

## Transportation
* http://www.crossrail.co.uk/route/near-you
* https://www.shipmap.org/
* http://dsl.richmond.edu/panorama/canals
* https://www.theguardian.com/world/ng-interactive/2014/aviation-100-years

## Weather (ALL cool, worth a new category)
* Niiiiice: https://www.windy.com/?51.546,-0.076,5
* Wind predictions: http://project-ukko.net/map.html
* https://volcano.si.axismaps.io/

## Tourism
* meh: http://www.alfoart.com/phototravel/
* https://www.londoncitybreak.com/map
* https://hleb-dom.ru/en/location.html
* https://www.insideasiatours.com/step-inside/

## Population
* http://luminocity3d.org/WorldCity/#5/11.502/25.093

## Music
* https://radio.garden/listen/river-93-1-krvn-fm/dt6VbZNX
* http://rollingstones.vizzuality.com/#/1

## Magic
* http://lotrproject.com/map
* http://www.welcometofillory.com/map0

## Activism
* http://www.therefugeeproject.org/#/2018
* http://dsl.richmond.edu/panorama/forcedmigration

## Pollution/env, inc. climate change
* http://www.impactlab.org/map/#usmeas=absolute&usyear=2020-2039&gmeas=absolute&gyear=1986-2005
* http://senseable.mit.edu/treepedia
* http://app.dumpark.com/seas-of-plastic-2/#oceans
* https://www.shipmap.org/
* http://arcticspills.wwf.ca/#home/

## Wildlife
* http://maps.tnc.org/migrations-in-motion

## Alt. energy
* ish? https://www.nongasmap.org.uk/
* http://project-ukko.net/map.html

## Conservation
* http://senseable.mit.edu/treepedia
* https://www.protectedplanet.net/marine

## Demographics
* Population: https://population.io/
* http://dsl.richmond.edu/panorama/foreignborn/#decade=2010
* http://metrocosm.com/global-migration-map.html

## History
* http://dsl.richmond.edu/panorama/foreignborn/#decade=2010&county=G2000110
* http://dsl.richmond.edu/panorama/overlandtrails
* https://www.theguardian.com/world/ng-interactive/2014/aviation-100-years

## Migration
* http://dsl.richmond.edu/panorama/overlandtrails
* http://www.therefugeeproject.org/#/2018
* http://metrocosm.com/global-migration-map.html
* http://dsl.richmond.edu/panorama/forcedmigration

## Hydro
* http://dsl.richmond.edu/panorama/canals

## Tons of cool everything
* https://www.kiln.digital/projects
