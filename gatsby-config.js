// Noyce:
// https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786
// NOTE: use an `.env` file locally but set env vars manually in Netlify:
// https://app.netlify.com/sites/abettermap/settings/deploys#environment
const dotenv = require('dotenv')

dotenv.config()

// Netlify contexts checks
// https://www.gatsbyjs.org/packages/gatsby-plugin-robots-txt/#netlify
const {
  NODE_ENV,
  URL: NETLIFY_SITE_URL = `https://www.abettermap.com`,
  DEPLOY_PRIME_URL: NETLIFY_DEPLOY_URL = NETLIFY_SITE_URL,
  CONTEXT: NETLIFY_ENV = NODE_ENV,
} = process.env
const isNetlifyProduction = NETLIFY_ENV === 'production'
const siteUrl = isNetlifyProduction ? NETLIFY_SITE_URL : NETLIFY_DEPLOY_URL

module.exports = {
  siteMetadata: {
    title: `A Better Map`,
    description: `A Better Map builds modern and responsive interactive web maps, map plugins, and map apps.`,
    author: `@abettermap`,
    tagline: `Modern maps for modern users`,
    siteUrl,
  },
  plugins: [
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        // Hide style guide from search engine indexing
        exclude: ['/secret-style-guide/', `/secret-style-guide`],
      },
    },
    `gatsby-plugin-react-helmet`,
    // Pieces and fragments such as home-content
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `content`,
        path: `${__dirname}/src/content/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: `${__dirname}/src/pages/blog/`,
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        defaultLayouts: {
          portfolio: require.resolve(
            './src/components/page-templates/portfolioPageTemplate.jsx'
          ),
          blog: require.resolve(
            './src/components/page-templates/blogPostTemplate.jsx'
          ),
          default: require.resolve(
            './src/components/page-templates/defaultPageTemplate.jsx'
          ),
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-abm-website`,
        short_name: `A Better Map`,
        start_url: `/`,
        // TODO: adjust or remove these things
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/logo/abm-logo-pins-only.svg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-env-variables`,
      options: {
        // All the Netlify env vars...
        // https://netlify.com/docs/continuous-deployment/#environment-variables
        whitelist: [
          `BRANCH`,
          `COMMIT_REF`,
          `CONTEXT`,
          `DEPLOY_PRIME_URL`,
          `DEPLOY_URL`,
          `HEAD`,
          `INCOMING_HOOK_BODY`,
          `INCOMING_HOOK_TITLE`,
          `INCOMING_HOOK_URL`,
          `PULL_REQUEST`,
          `REPOSITORY_URL`,
          `REVIEW_ID`,
          `URL`,
          `NODE_ENV`, // for html.js
        ],
      },
    },
    {
      // Build SEO-friendly robots.txt in production, omit from other contexts.
      // https://www.gatsbyjs.org/packages/gatsby-plugin-robots-txt/#netlify
      // https://www.netlify.com/docs/continuous-deployment/#deploy-contexts
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        // sitemap: `https://www.example.com/sitemap.xml`,
        resolveEnv: () => NETLIFY_ENV,
        env: {
          production: {
            policy: [{ userAgent: '*', disallow: ['/secret-style-guide'] }],
          },
          'branch-deploy': {
            policy: [{ userAgent: '*', disallow: ['/'] }],
            sitemap: null,
            host: null,
          },
          'deploy-preview': {
            policy: [{ userAgent: '*', disallow: ['/'] }],
            sitemap: null,
            host: null,
          },
        },
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: ['Questrial', 'Radley', 'Raleway:600'],
        display: 'swap',
      },
    },
    // Logos and icons
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          // NOTE: need to flip the slashes on Windows
          include: /src\/images/,
        },
      },
    },
    // Files in ./src/pages get converted into pages automatically
    // WOW: order definitely matters! Putting the pages FS config at top causes
    // portfolio sub-pages to not render. Good to know...
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `portfolio`,
        path: `${__dirname}/src/pages/portfolio`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages/`,
      },
    },
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-breadcrumb`,
      options: {
        // useAutoGen: required 'true' to use autogen
        useAutoGen: true,
        // exlude: optional, include to overwrite these default excluded pages
        exclude: [`/dev-404-page`, `/404`, `/404.html`],
      },
    },
    // QUESTION: what is this for?
    // {
    //   resolve: 'gatsby-plugin-page-creator',
    //   options: {
    //     path: `${__dirname}/src/includes/`,
    //   },
    // },
    // this (optional) plugin enables Progressive Web App + Offline
    // functionality To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
