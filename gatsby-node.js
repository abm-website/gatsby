/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const { createFilePath } = require(`gatsby-source-filesystem`)

// Implement the Gatsby API “onCreatePage”, called after each page is created.
// exports.onCreatePage = ({ page, actions }) => {}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  // `slug` field as defined in frontmatter of MD(X) files. Home content and any
  // other partials will not be used for defining paths but rather just for more
  // specific queries like the one/s in index.js.
  if (node.internal.type === 'Mdx' || node.internal.type === 'MarkdownRemark') {
    const autoGenSlugBasedOnPath = createFilePath({
      node,
      getNode,
      trailingSlash: false,
    })

    createNodeField({
      node,
      name: 'slug',
      value: autoGenSlugBasedOnPath,
    })
  }
}
