---
name: Sleeping Bear Heritage Trail
intro: >
  This interactive map showcases the Sleeping Bear Heritage Trail (SBHT), a 
  popular multi-use pathway running through Sleeping Bear Dunes National 
  Lakeshore in northwest Lower Michigan.
location:
  name: NW Lower Michigan
  latitude: 44
  longitude: -80
sort_order: 1
logo: sbht-logo-no-text.png
featured_img: select-a-season.jpg
project_url: https://friendsofsleepingbear.org/sbht-i-map
customer: Friends of Sleeping Bear Dunes
customer_url: https://friendsofsleepingbear.org
img_dir: portfolio/sbht
# how to validate tags?
tech_tags:
  - AngularJS
  - Carto
  - JavaScript
  - SCSS
  - PHP
  - Leaflet
industry_tags:
  - Nonprofit
  - Recreation
feature_tags:
  - Responsive
  - Client-managed data
  - Shareable
  - Theme-matching
map_features:
  - heading: Responsive design
    description: >
      The SBHT map was designed with a variety of platforms in mind so it looks 
      great on a variety of devices.
    img: sbht-responsive.png
  - heading: Client-managed data
    description: >
      The map is powered by a spatial database and we provided the client with 
      comprehensive training so that they could manage their own content. 
    img: client-driven-data.jpg
  - heading: Direct links to map features
    description: >
      We added direct-linking that allows users to visit specific points via
      unique URLs. This also allowed the client to make a few bucks by <a 
      href="https://friendsofsleepingbear.org/sleeping-bear-heritage-trail/get-on-the-map/" title="SBHT map vendor ad spot info" target="_blank" rel="noopener noreferrer"> 
      selling paid spots </a> to local businesses.
    img: sbht-direct-url.jpg
  - heading: Social media sharing
    description: >
      Sleeping Bear Dunes receives over 1 million tourists a year so spreading 
      the word about the trail is important to tourism. The map has direct 
      links for sharing feature and location info on a variety of platforms.
    img: sbht-sharing.jpg
  - heading: Four-season queries
    description: >
      Michigan is a dynamic landscape that varies greatly throughout the year. 
      To capture those changes a filter was added to show seasonal photos and 
      availability of amenities in each season.
    img: select-a-season.jpg
  - heading: Dynamic informational popups
    description: >
      The SBHT interactive map is packed with diverse information, and the 
      popups that communicate it are well-styled and full of options.
    img: cherry-republic.jpg
  - heading: "\"Walk the trail\""
    description: >
      Thanks to the Friends’ project manager who captured photos along the 
      entire in all four seasons, map users can essentially “walk” the trail.
    img: walk-the-trail.jpg
  - heading: QR code compatibility
    description: >
      The map can start centered on a lat/long contained in the URL (e.g. like 
      <a href="https:// friendsofsleepingbear.org/sbht-i-map/position/44.85843/-86.06668" title="Exact latitude longitude point" target="_blank" rel="noopener noreferrer">this 
      point</a>). This way users can start the map at a location defined in a 
      QR code by scanning it with their smartphone.
    img: sbht-qr-code.jpg
  - heading: Intuitive map controls
    description: >
      In addition to standard map navigation controls, we added handy tools 
      like “Zoom to Previous” and a geolocation button for zooming to the 
      user’s current location.
    img: intuitive-ctrls.jpg
    # TODO: if you can get a pic for this, add it.
    #   - heading: Kiosk/screensaver mode
    #     description: >
    #       The Friends received permission from the National Park Service to use a kiosk located in the NPS visitor center as a public home for the SBHT map. To make things more interesting, A Better Map added “screensaver” functionality that starts a randomized tour of map locations after 3 minutes of idleness. If you feel like waiting that long and want to check it out, <a href="https://friendsofsleepingbear.org/sbht-i-map/kiosk.php" title="SBHT map kiosk page" target="_blank" rel="noopener noreferrer">go here</a> and don’t touch anything for 3 minutes. :)
    #     img: ""
---
