---
name: 1947 Archive Story Map
intro: >
  This map chronicles thousands of stories from individuals present during the 
  1947 British Partition of India. Originally developed by a team at Silver 
  Biology, the map has undergone some interesting enhancements by A Better Map.
location:
  name: Hmmmm
  latitude: -1
  longitude: -1
sort_order: 3
logo: 1947-logo.png
featured_img: migration-paths.jpg
project_url: https://in.1947partitionarchive.org/
customer: The 1947 Partition Archive
customer_url: https://www.1947partitionarchive.org
img_dir: portfolio/1947
# how to validate tags?
tech_tags:
  - AngularJS
  - jQuery
  - PHP
  - Google Maps API
  - Carto
industry_tags:
  - Nonprofit
  - Cultural
feature_tags:
  - Client-driven data
  - Shareable
  - Theme-matching
map_features:
# TODO: GIFs for anything relevant
  - heading: User interface enhancements
    description: >
      A Better Map added some stylish toggle buttons to the map interface as a 
      way to filter stories based on interviewees' origin, destination, or 
      current place of residence.
    img: ui-enhancements.jpg
  - heading: Animated migration paths
    description: >
      In order to visualize interviewees' migration patterns, we added animated lines and arrows that are triggered when a map marker is clicked.
    img: migration-paths.jpg
  - heading: Enhanced search options
    description: >
      In addition to clicking on map features, users can query the project 
      database as they type thanks to a categorized autocomplete list that 
      dynamically displays cities and interviewee names.
    img: enhanced-search.jpg
  - heading: Advanced sharing functionality
    description: >
      When we started helping on this project there were just over 100 stories. As that number grew into thousands we added a way to share individual stories through unique dynamic URLs.
    img: routing-indiv-story.jpg
  - heading: Sharing options
    description: >
      Dynamic URLs allow sharing of individual stories via <a href="https://www.facebook.com/1947PartitionArchive" title="1947 Partition Archive 
      Facebook page" target="_blank" rel="noopener noreferrer">1947's Facebook page</a> and its following 
      of nearly a million users. We added sharing buttons for a variety of 
      popular social media platforms including Facebook, Twitter, and Pinterest.
    # TODO: if/when website works again, get better screenshot of share btns
    img: routing-indiv-story.jpg
    # TODO: if/when website ever works again, get screenshot of FB post preview
    #   - heading: Social-media-friendly post content (RELATED TO ABOVE)
    #     description: Additionally, A Better Map coded the map to automatically populate key aspects of posts, tweets, pins, etc. For example, the Facebook post preview includes each interviewee's name, photo (when available), story excerpt, and direct link to that exact story.
    #     img: ""
---
