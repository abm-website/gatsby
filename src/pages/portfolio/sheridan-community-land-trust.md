---
name: Sheridan Recreation Map
intro: >
  Sheridan Community Land Trust started with a paper map of recreation 
  activities around Sheridan, Wyoming, and ended up with an engaging 
  interactive product. Combined with SCLT's well-written content and 
  captivating images, the map delivers an experience not possible through paper 
  or PDF.
location:
  name: Sheridan, Wyoming
  latitude: -1
  longitude: -1
sort_order: 2
logo: sclt-logo.jpg
featured_img: sclt-theme-match.jpg
project_url: https://sheridanclt.org/recreation-map
customer: Sheridan Community Land Trust
customer_url: https://sheridanclt.org
img_dir: portfolio/sclt
# how to validate tags?
tech_tags:
  - jQuery
  - OpenLayers
industry_tags:
  - Nonprofit
  - Recreation
  - Conservation
feature_tags:
  - Responsive
  - Client-driven data
  - Theme-matching
map_features:
  - heading: Interactivity
    description: >
      SCLT's interactive map was designed with user interaction as a top 
      priority, including a day trip activity filter, dynamic popups, and 
      colorful map legend controls.
    img: interactivity.jpg
  - heading: Responsiveness
    description: >
      The SCLT map was designed to look and work great on a variety of 
      platforms and devices regardless of resolution, browser, or operating 
      system.
    img: sclt-responsive.jpg
  - heading: Supplemental layers
    description: >
      In addition to its main focus on recreation, the map also included 
      additional layers like Forest Service features, fishing spots, and SCLT 
      conservation easements.
    img: supplemental-layers.jpg
  - heading: Integration
    description: >
      Paying careful attention to the existing SCLT website, the map was 
      created with subtle layout details in mind. We also took steps to ensure 
      compatibility with the site's WordPress theme.
    img: sclt-theme-match.jpg
---
