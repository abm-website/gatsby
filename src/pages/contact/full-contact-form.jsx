import React from 'react'

import ContactPage from './index'

export default ({ pageContext }) => (
  <ContactPage pageContext={pageContext} includeDeets />
)
