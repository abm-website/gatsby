import styled from 'styled-components'

// TODO: use Rebass Button instead
const StyledBurger = styled.button`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 2rem;
  height: 2rem;
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0;
  z-index: 10;

  @media (min-width: ${({ theme }) => theme.breakpoints[2]}) {
    display: none;
  }

  &:focus {
    outline: none;
  }

  div {
    background: ${({ theme }) => theme.colors.white};
    border-radius: 10px;
    height: 0.175rem;
    transform-origin: 1px;
    transition: all 0.25s linear;
    width: 2rem;

    :first-child {
      width: ${({ open }) => (open ? '2rem' : '1.5rem')};
      transform: ${({ open }) => (open ? 'rotate(45deg)' : 'rotate(0)')};
    }

    :nth-child(2) {
      width: 1.75rem;
      opacity: ${({ open }) => (open ? '0' : '1')};
      transform: ${({ open }) => (open ? 'translateX(20px)' : 'translateX(0)')};
    }

    :nth-child(3) {
      transform: ${({ open }) => (open ? 'rotate(-45deg)' : 'rotate(0)')};
      width: 2rem;
    }
  }
`

export default StyledBurger
