import React, { useState } from 'react'

import { Box, Text, Flex } from 'rebass/styled-components'

const ToggleableSection = ({
  children,
  asParent = 'section',
  asHeading = 'h3',
  title,
  openByDefault,
  sx = { mb: 9 },
}) => {
  const [open, setOpen] = useState(openByDefault)

  return (
    <Box as={asParent} sx={{ ...sx }}>
      <Flex
        as={asHeading}
        open={open}
        onClick={() => setOpen(!open)}
        alignItems="center"
        style={{ cursor: 'pointer' }}
      >
        <Text mr={1} color="primary.1">
          {open ? '\u2296' : '\u2295'}
        </Text>
        {title}
      </Flex>
      <div
        style={{
          height: 'auto',
          maxHeight: open ? 3000 : 0,
          overflow: 'hidden',
          transition: '300ms max-height',
        }}
      >
        {children}
      </div>
    </Box>
  )
}

export default ToggleableSection
