import React from 'react'
import { Link } from 'rebass/styled-components'

import { convertToValidId } from '../../helpers/helpers'

const TableOfContents = ({ sections }) => (
  <nav>
    <ul>
      {sections.map(({ heading }) => (
        <li key={heading}>
          <Link href={`#${convertToValidId(heading)}`} title="heading">
            {heading}
          </Link>
        </li>
      ))}
    </ul>
  </nav>
)

export default TableOfContents
